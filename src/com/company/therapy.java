package com.company;

public class Therapy extends Main {

    String name;
    int hp;
    int proc1 = 20;
    int proc2 = 30;
    int proc3 = 40;

    public Therapy(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    @Override
    public String toString() {
        return "Пациент {" +
                "name='" + name + '\'' +
                ", hp=" + hp + '}';
    }

    public void getProc1() {
        this.hp += proc1;
    }

    public void getProc2() {
        this.hp += proc2;
    }

    public void getProc3() {
        this.hp += proc3;
    }


    public int getHp() {
        return hp;
    }


}
